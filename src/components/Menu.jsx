import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Link} from 'react-router-dom';
import { Navbar, Nav, Button, Form, FormControl } from 'react-bootstrap';

function Menu() {
    return (
        <div>
            <Navbar bg="dark" expand="lg" variant="dark">
                <div className="container">
                    <Navbar.Brand disabled style={{ pointerEvents: 'none' }} >AMS</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                            <Nav.Link as={Link} to='/'>Home</Nav.Link>
                        </Nav>
                        <Form inline>
                            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                            <Button variant="outline-info">Search</Button>
                        </Form>
                    </Navbar.Collapse>
                </div>
            </Navbar>
        </div>
    )
}

export default Menu
