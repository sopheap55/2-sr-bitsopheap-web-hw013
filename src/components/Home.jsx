import React, { Component } from 'react';
import {Button} from 'react-bootstrap';
import DataTable from './DataTable';
import {Link} from 'react-router-dom';

export default class Home extends Component {
    render() {
        return (
            <div className="container addBtn">
                <h2>Article Management</h2>
                <Button variant="dark" as={Link} to="/add" >Add New Article</Button>{' '}
                <DataTable />
            </div>
        )
    }
}
