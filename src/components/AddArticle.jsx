import React, { Component } from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import axios from 'axios';
import swal from 'sweetalert';

export default class AddArticle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            TITLE: " ",
            DESCRIPTION: " ",
            IMAGE: "https://omegamma.com.au/wp-content/uploads/2017/04/default-image.jpg",
        }
    }
    
    changeHandle = (event) => {
        this.setState({ [event.target.name]: event.target.value })
    }
    submitHandle = (event) => {
        event.preventDefault();
        swal({
            title: "Do you want to save it?",
            buttons: ["Don't save", "Save"],
            // dangerMode: "Save",
        }).then((willSave) => {
            if (willSave) {
                axios.post("http://110.74.194.124:15011/v1/api/articles", this.state).then((res) => {
                    swal("Save data successfully",
                        { icon: "success" });
                    
                })
            } else {
                swal("Not save!");
            }
        })

    }
    render() {
        const { TITLE, DESCRIPTION } = this.state;
        return (
            <div className="container add">
                <h2>Add Article</h2>
                <div className="form-left">
                    <Form onSubmit={this.submitHandle}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>TITLE</Form.Label>
                            <Form.Control name="TITLE" value={TITLE} type="title" onChange={this.changeHandle} placeholder="Enter title" />
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Description</Form.Label>
                            <Form.Control name="DESCRIPTION" value={DESCRIPTION} type="description" onChange={this.changeHandle} placeholder="Enter description" />
                        </Form.Group>
                        <Button variant="primary" type="submit" >
                            Submit
                        </Button>
                    </Form>
                </div>
                <div className="img-right">
                    <Card style={{ width: '18rem' }}>
                        <Card.Img variant="top" src="https://omegamma.com.au/wp-content/uploads/2017/04/default-image.jpg" />
                    </Card>
                </div>
            </div>
        )
    }
}



// class Submit extends Component {

//     render() {
//         return (
//             <div>

//             </div>
//         );
//     }
// }



