import React, { Component } from 'react'
import { Table } from 'react-bootstrap'
import axios from 'axios';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import swal from 'sweetalert';

export default class DataTable extends Component {
    constructor() {
        super();
        this.state = {
            users: [],
        };
    }
    componentDidMount() {
        this.getData();
    }
    getData() {
        axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15").then((res) => {
            this.setState({
                users: res.data.DATA,
            })

        })
    }
    
    deleteData(id) {
        swal({
            title: "Are you sure to delete?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                fetch("http://110.74.194.124:15011/v1/api/articles/" + id, {
                    method: "DELETE",
                }).then((resl) => {
                    resl.json().then((resp) => {
                        // alert(resp.MESSAGE);
                        swal("Your data has been deleted!", {
                            icon: "success",
                        });
                        this.getData();
                    })
                })
            } else {
                swal("Your data is safe!");
            }
        });
    }
    render() {
        let showData = this.state.users.map(user => {
            return <tr key={user.ID}>
                <td>{user.ID}</td>
                <td className="titleTb">{user.TITLE}</td>
                <td>{user.DESCRIPTION}</td>
                <td>{user.CREATED_DATE}</td>
                <td><img src={user.IMAGE} alt="" className="image" /></td>
                <td className="actionTb">
                    <Button variant="primary" as={Link} to={`/view/${user.ID}`}>View</Button>{' '}
                    <Button variant="warning">Edit</Button>{' '}
                    <Button variant="danger" onClick={() => this.deleteData(user.ID)}>Delete</Button>
                </td>
            </tr>
        })
        return (
            <div className="table">
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>TITLE</th>
                            <th>DESCRIPTION</th>
                            <th>CREATE DATE</th>
                            <th>IMAGE</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody className="tbo">
                        {showData}
                    </tbody>
                </Table>
            </div>
        )
    }
}


