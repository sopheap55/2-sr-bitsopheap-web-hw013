import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import axios from 'axios';

export default class View extends Component {
    constructor() {
        super();
        this.state = {
            users: [],
        };
    }
    componentDidMount() {
        this.getData();
    }
    getData() {
        axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15").then((res) => {
            this.setState({
                users: res.data.DATA,
            })

        })
    }
    view(id) {
        fetch("http://110.74.194.124:15011/v1/api/articles/" + id, {
            method: "GET",
        }).then((resl) => {
            resl.json().then((resp) => {
                this.getData();
            })
        })
    }
    render() {
        let viewData = this.state.users.map((user) => {
            return (
                <div key={user.ID}>
                    <div className="image-left">
                        <Card.Img variant="top" src={user.IMAGE} />
                    </div>
                    <div className="cap-right">
                        <Card.Body>
                            <Card.Title>{user.TITLE}</Card.Title>
                            <Card.Text>
                                {user.DESCRIPTION}
                            </Card.Text>
                        </Card.Body>
                    </div>
                </div>
            )
        })
        return (
            <div className="container con">
                <h1>Article</h1>
                {viewData}
                
            </div >
        );
    }
}
