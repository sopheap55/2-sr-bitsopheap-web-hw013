import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Menu from './components/Menu';
import Home from './components/Home';
import NotFound from './components/NotFound';
import View from './components/View';
import AddArticle from './components/AddArticle';


function App() {
  return (
    <div className="App">
      <Router>
        <Menu />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/view" component={View}/>
          <Route path="/add" component={AddArticle}/>
          <Route path="*" component={NotFound} />
        </Switch>
      </Router>

    </div>
  );
}

export default App;
